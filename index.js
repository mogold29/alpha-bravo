const parse = require('./parse');
const numbers = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "zero"];

module.exports = {
     returnAsString(input) {
        if(!input) {
            return 'error';
        }
        return parse(input)
    },
    returnAsHTML(input) {
        if(!input) {
            return 'error';
        }
        let stringToParse = parse(input);
        let retunedStringToArray = stringToParse.split(" ");
        let stringToReturn = "<p>";

        retunedStringToArray.forEach(word => {
            let newWord = word.toUpperCase();
            if(word.length > 0 && word === newWord) {
                stringToReturn += `<b>${word}</b> `;
            } else if(numbers.includes(word)) {
                stringToReturn += `<i>${word}</i> `;
            } else {
                stringToReturn += `${word} `;
            }

        });

        stringToReturn += "</p>";
        
        return stringToReturn;
    }
}