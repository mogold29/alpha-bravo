// dictinary of letters (lowercase and uppercase) and numbers
const dict = {
    a: "alpha",
    b: "bravo",
    c: "charlie",
    d: "delta",
    e: "echo",
    f: "foxtrot",
    g: "golf",
    h: "hotel",
    i: "india",
    j: "juliet",
    k: "kilo",
    l: "lima",
    m: "mike",
    n: "november",
    o: "oscar",
    p: "papa",
    q: "quebec",
    r: "romeo",
    s: "sierra",
    t: "tango",
    u: "uniform",
    v: "victor",
    w: "whiskey",
    x: "xray",
    y: "yankee",
    z: "zulu",
    A: "ALPHA",
    B: "BRAVO",
    C: "CHARLIE",
    D: "DELTA",
    E: "ECHO",
    F: "FOXTROT",
    G: "GOLF",
    H: "HOTEL",
    I: "INDIA",
    J: "JULIET",
    K: "KILO",
    L: "LIMA",
    M: "MIKE",
    N: "NOVEMBER",
    O: "OSCAR",
    P: "PAPA",
    Q: "QUEBEC",
    R: "ROMEO",
    S: "SIERRA",
    T: "TANGO",
    U: "UNIFORM",
    V: "VICTOR",
    W: "WHISKEY",
    X: "XRAY",
    Y: "YANKEE",
    Z: "ZULU",
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine",
    0: "zero"
}

// popular domains to check when parsing emails
const domains = [
    "aol.com", 
    "att.net", 
    "comcast.net", 
    "facebook.com", 
    "gmail.com", 
    "gmx.com", 
    "googlemail.com",
    "google.com", 
    "hotmail.com", 
    "hotmail.co.uk", 
    "mac.com", 
    "me.com", 
    "mail.com", 
    "msn.com",
    "live.com", 
    "sbcglobal.net", 
    "verizon.net", 
    "yahoo.com", 
    "yahoo.co.uk", 
    "email.com", 
    "fastmail.fm", 
    "games.com", 
    "gmx.net", 
    "hush.com", 
    "hushmail.com", 
    "icloud.com",
    "iname.com", 
    "inbox.com", 
    "lavabit.com", 
    "love.com" , 
    "outlook.com", 
    "pobox.com", 
    "protonmail.ch", 
    "protonmail.com", 
    "tutanota.de", 
    "tutanota.com", 
    "tutamail.com", 
    "tuta.io",
    "keemail.me", 
    "rocketmail.com", 
    "safe-mail.net", 
    "wow.com", 
    "ygm.com",
    "ymail.com", 
    "zoho.com", 
    "yandex.com",
    "bellsouth.net", 
    "charter.net", 
    "cox.net", 
    "earthlink.net", 
    "juno.com",
    "btinternet.com", 
    "virginmedia.com", 
    "blueyonder.co.uk", 
    "freeserve.co.uk", 
    "live.co.uk",
    "ntlworld.com", 
    "o2.co.uk", 
    "orange.net", 
    "sky.com", 
    "talktalk.co.uk", 
    "tiscali.co.uk",
    "virgin.net", 
    "wanadoo.co.uk", 
    "bt.com",
    "sina.com", 
    "sina.cn", 
    "qq.com", 
    "naver.com", 
    "hanmail.net", 
    "daum.net", 
    "nate.com", 
    "yahoo.co.jp", 
    "yahoo.co.kr", 
    "yahoo.co.id", 
    "yahoo.co.in", 
    "yahoo.com.sg", 
    "yahoo.com.ph", 
    "163.com", 
    "yeah.net", 
    "126.com", 
    "21cn.com", 
    "aliyun.com", 
    "foxmail.com",
    "hotmail.fr", 
    "live.fr", 
    "laposte.net", 
    "yahoo.fr", 
    "wanadoo.fr", 
    "orange.fr", 
    "gmx.fr", 
    "sfr.fr", 
    "neuf.fr", 
    "free.fr",
    "gmx.de", 
    "hotmail.de", 
    "live.de", 
    "online.de", 
    "t-online.de", 
    "web.de", 
    "yahoo.de",
    "libero.it", 
    "virgilio.it", 
    "hotmail.it", 
    "aol.it", 
    "tiscali.it", 
    "alice.it", 
    "live.it", 
    "yahoo.it", 
    "email.it", 
    "tin.it", 
    "poste.it", 
    "teletu.it",
    "mail.ru", 
    "rambler.ru", 
    "yandex.ru", 
    "ya.ru", 
    "list.ru", 
    "hotmail.be", 
    "live.be", 
    "skynet.be", 
    "voo.be", 
    "tvcablenet.be", 
    "telenet.be",
    "hotmail.com.ar", 
    "live.com.ar", 
    "yahoo.com.ar", 
    "fibertel.com.ar", 
    "speedy.com.ar", 
    "arnet.com.ar", 
    "yahoo.com.mx", 
    "live.com.mx", 
    "hotmail.es", 
    "hotmail.com.mx", 
    "prodigy.net.mx",
    "yahoo.com.br", 
    "hotmail.com.br", 
    "outlook.com.br", 
    "uol.com.br", 
    "bol.com.br", 
    "terra.com.br", 
    "ig.com.br", 
    "itelefonica.com.br", 
    "r7.com", 
    "zipmail.com.br", 
    "globo.com", 
    "globomail.com", 
    "oi.com.br"
  ];

module.exports = function parse(input) {
    // email regex
    const emailRegEx = RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)
    // string to array to do checks
    const array = Array.from(input);
    // second array to keep intact for later use
    const secondArray = Array.from(input);
    // index position in array of the @ symbol
    let atPostitionInEmailAddress = 0;
    // index position of the first dot in the domain
    let dotPositionIndex = 0;
    // empty variable for .com, .co.uk, .eu
    let dotString = "";
    // empty varibale for the output
    let output = "";

    // check if input is an email
    if(emailRegEx.test(input) == true) {
        // find the position of the @ symbol
        array.find((position, index) => {
            if(position == "@") {
                // set the position of the @ symbol in the input array
                atPostitionInEmailAddress = index;
            }
        });

        // get just the domain and the .com from the input
        let domainStringArray = array.slice(atPostitionInEmailAddress +1);
        // join the array to a string and remove all commas
        let domainString = domainStringArray.join().replace(/,/g, "");
        // get the username from the email address
        let originalArray = secondArray.slice(0, atPostitionInEmailAddress);

        // generate the output for the username
        generateOutput(originalArray);

        // if the domain is included in the domains array return the domain as a whole word instead of breaking it up
        if(domains.includes(domainString)) {
            // add the domain prepended with an @ symbol to the output string
            output += `@${domainString}`;
        } else {
        // find the postion of the first dot in the domain
        let dotPosition = domainStringArray.join().replace(/,/g, "").indexOf(".");
        // join the domain array and remove all commas
        dotString = domainStringArray.slice(dotPosition).join().replace(/,/g, "");
        // add to the output string prepended with the @ symbol and remove the .com .co.uk. eu
        generateOutput(["@",...domainStringArray.slice(0, dotPosition)]);
        }
        // if the string is not an email just generate output of the full string
    } else if (emailRegEx.test(input) == false) {
        generateOutput(array)
    }

    // generate output
    function generateOutput(array) {
        // for each value in the input array 
        array.forEach((value) => {
            // if the value of the input is not in a letter or number return the value as is eithout changing it
            if(dict[value] == undefined) {
                output += value + " ";
            } else {
                // if the value is a letter or number return the value from the dictonary
                output += dict[value] + " ";
            }
        });
    }

    return (output + dotString);
}
