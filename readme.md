# Alpha Bravo

Alpha Bravo parses a string and returns the NATO phonetic word for each charcter. 


### Installation
```sh
$ npm install alpha-bravo
```

### Usage

```sh
const abc = require('alpha-bravo');
```

Alpha Bravo returns the text either as a simple text string or as a HTML formatted string

### Example


#### Return as string
###
```sh
const string = abc.returnAsString('alpha-bravo');
```

##### Result is a simple string

alpha lima papa hotel alpha - bravo romeo alpha victor oscar
#### Return as HTML
###
```sh
const string = abc.returnAsHTML('UPPERlower123');
```

##### Result is formatted HTML with uppercase letters bold and numbers italic

**UNIFORM PAPA PAPA ECHO ROMEO** lima oscar whiskey echo romeo *one two three*

### Parsing Email Address
When encoutnering an email address alpha-bravo checks against a list of popular email domains and returns the domain as one word instead of breaking it down to seperate words
```sh
const string = abc.returnAsString('test@gmail.com');
```
###
Returns tango echo sierra tango @gmail.com

```sh
const string = abc.returnAsString('test@domain.com');
```
###
Returns tango echo sierra tango @ delta oscar mike alpha india november .com


